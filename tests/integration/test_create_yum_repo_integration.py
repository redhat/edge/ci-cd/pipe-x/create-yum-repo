"""Tests for create_yum_repo.create_yum_repo"""

import sys
from pathlib import Path
from unittest.mock import patch, call

import pytest

from create_yum_repo.create_yum_repo import main
from create_yum_repo.download import DEFAULT_KOJI_INSTANCES, SUPPORTED_ARCHES


COMPOSES_URL_PREFIX = (
    "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose"
)
MIRRORS_URL_PREFIX = "http://mirror.stream.centos.org/9-stream"


@pytest.mark.asyncio
@patch.object(
    sys,
    "argv",
    [
        "create-yum-repo",
        "cs9",
        "-l",
        "../resources/fake_lockfile.lock.json",
        "-o",
        "/tmp/test_create_yum_repo",
        "-r",
        f"{MIRRORS_URL_PREFIX}/{{centos_repo}}/{{arch}}/os/",
        "-r",
        f"{MIRRORS_URL_PREFIX}/{{centos_repo}}/source/tree/",
        "-r",
        f"{COMPOSES_URL_PREFIX}/{{centos_repo}}/{{arch}}/os/",
        "-r",
        f"{COMPOSES_URL_PREFIX}/{{centos_repo}}/source/tree/",
    ],
)
@patch("create_yum_repo.create_yum_repo.build_nvra_lookup")
async def test_main(
    mock_build_nvra_lookup,
    mock_nvra_lookup,
):
    """Test build_nvra_lookup"""
    expected_base_distro = "cs9"
    expected_lockfile = Path("../resources/fake_lockfile.lock.json")
    expected_output_dir = Path("/tmp/test_create_yum_repo")
    expected_repo_priorities = {
        "mirror.stream.centos.org": 1,
        "composes.stream.centos.org": 3,
    }

    expected_repo_urls = {
        f"{MIRRORS_URL_PREFIX}/BaseOS/x86_64/os/",
        f"{COMPOSES_URL_PREFIX}/BaseOS/x86_64/os/",
        f"{MIRRORS_URL_PREFIX}/AppStream/x86_64/os/",
        f"{COMPOSES_URL_PREFIX}/AppStream/x86_64/os/",
        f"{MIRRORS_URL_PREFIX}/CRB/x86_64/os/",
        f"{COMPOSES_URL_PREFIX}/CRB/x86_64/os/",
        f"{MIRRORS_URL_PREFIX}/BaseOS/aarch64/os/",
        f"{COMPOSES_URL_PREFIX}/BaseOS/aarch64/os/",
        f"{MIRRORS_URL_PREFIX}/AppStream/aarch64/os/",
        f"{COMPOSES_URL_PREFIX}/AppStream/aarch64/os/",
        f"{MIRRORS_URL_PREFIX}/CRB/aarch64/os/",
        f"{COMPOSES_URL_PREFIX}/CRB/aarch64/os/",
        f"{MIRRORS_URL_PREFIX}/BaseOS/source/tree/",
        f"{MIRRORS_URL_PREFIX}/AppStream/source/tree/",
        f"{MIRRORS_URL_PREFIX}/CRB/source/tree/",
        f"{COMPOSES_URL_PREFIX}/BaseOS/source/tree/",
        f"{COMPOSES_URL_PREFIX}/AppStream/source/tree/",
        f"{COMPOSES_URL_PREFIX}/CRB/source/tree/",
    }

    mock_build_nvra_lookup.return_value = mock_nvra_lookup

    with patch(
        "create_yum_repo.create_yum_repo.create_repo"
    ) as mock_create_repo, patch(
        "create_yum_repo.create_yum_repo.create_index_html"
    ) as mock_create_index, patch(
        "create_yum_repo.create_yum_repo.download_packages"
    ) as mock_download_packages:
        await main()

    mock_build_nvra_lookup.assert_called_once_with(
        expected_repo_urls, expected_repo_priorities
    )
    mock_download_packages.assert_called_once_with(
        expected_base_distro,
        expected_lockfile,
        mock_nvra_lookup,
        DEFAULT_KOJI_INSTANCES,
        expected_output_dir,
    )

    expected_create_repo_calls = [
        call(expected_output_dir / expected_base_distro / arch / "os")
        for arch in SUPPORTED_ARCHES
    ]
    mock_create_repo.assert_has_calls(expected_create_repo_calls, any_order=True)
    mock_create_index.assert_called_once_with(expected_output_dir)
