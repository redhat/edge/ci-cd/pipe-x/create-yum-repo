"""Test create_yum_repo.sanity_test"""
import sys
import subprocess
from pathlib import Path
from unittest.mock import patch, call

import pytest

from create_yum_repo import sanity_test
from create_yum_repo.sanity_test import (
    download_pkg_with_plus,
    download_source_packages,
    glob_for_rpm,
    verify_repo,
)


@pytest.mark.asyncio
async def test_verify_repo():
    """Test verify_repo"""
    with patch(
        "create_yum_repo.sanity_test.dnf_makecache"
    ) as mock_dnf_makecache, patch(
        "create_yum_repo.sanity_test.download_pkg_with_plus"
    ) as mock_download_pkg_with_plus, patch(
        "create_yum_repo.sanity_test.dnf_download_package"
    ) as mock_dnf_download_package, patch(
        "create_yum_repo.sanity_test.download_source_packages"
    ) as mock_download_source_packages:
        await verify_repo("file://fake.repo", "x86_64")

    mock_dnf_makecache.assert_called()
    mock_download_pkg_with_plus.assert_called()
    mock_dnf_download_package.assert_called()
    mock_download_source_packages.assert_called()


def test_download_source_packages():
    """Test downloading_source_packages"""
    with patch(
        "create_yum_repo.sanity_test.dnf_download_package"
    ) as mock_dnf_download_package, patch(
        "create_yum_repo.sanity_test.glob_for_rpm"
    ) as mock_glob_for_rpm, patch(
        "create_yum_repo.sanity_test.rpm_checksig"
    ) as mock_rpm_checksig:
        download_source_packages(Path("./mockconf"), Path("./mockdest"))

    mock_dnf_download_package.assert_called()
    mock_glob_for_rpm.assert_called()
    mock_rpm_checksig.assert_called()


def test_glob_for_rpm(tmp_path):
    """Test glob_for_rpm"""
    fake_rpm = tmp_path / "fake-rpm-1.0-16.el9.aarch64.rpm"
    fake_rpm.touch()

    fake_src_rpm = tmp_path / "fake-rpm-1.0-16.el9.src.rpm"
    fake_src_rpm.touch()

    assert glob_for_rpm(str(tmp_path), "non-existent-rpm", "aarch64") is None
    assert glob_for_rpm(str(tmp_path), "fake-rpm", "src") == fake_src_rpm


def test_entrypoint():
    """Test run"""
    repo = "https://fake-repo"
    with patch.object(sys, "argv", ["repo-sanity-test", repo]), patch(
        "create_yum_repo.sanity_test.verify_repo"
    ) as mock_verify_repo:
        sanity_test.run()

    mock_verify_repo.assert_has_calls(
        [call(repo, arch) for arch in ("aarch64", "x86_64")]
    )


def test_download_pkg_with_plus():
    """Test download_pkg_with_plus"""
    command = "curl --fail http://fake.url/repos/cs9/aarch64/os/Packages/gcc-c++-11.3.1-2.1.el9.aarch64.rpm"  # pylint: disable=line-too-long
    expected = command.split()
    with patch(
        "create_yum_repo.external_commands.subprocess.run"
    ) as mock_subprocess_run, patch(
        "create_yum_repo.sanity_test._find_pkg_name"
    ) as mock_find_pkg_name:
        mock_find_pkg_name.return_value = "gcc-c++-11.3.1-2.1.el9.aarch64.rpm"

        download_pkg_with_plus("http://fake.url/repos/cs9", "aarch64")

        mock_subprocess_run.assert_called_once_with(
            expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
        )
