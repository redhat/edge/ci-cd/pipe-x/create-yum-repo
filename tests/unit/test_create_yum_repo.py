"""Test create_yum_repo.create_yum_repo"""
import pytest

from create_yum_repo.create_yum_repo import (
    _repository_priorities,
    _expand_repository_urls,
)

CENTOS_MIRROR_BASE_URL = "http://mirror.stream.centos.org/9-stream"
CENTOS_DEVELOPMENT_COMPOSE_BASE_URL = (
    "https://composes.stream.centos.org/development/latest-CentOS-Stream/compose"
)


@pytest.mark.parametrize(
    "mirror_list,expected",
    [
        ([], {}),
        (
            [
                CENTOS_MIRROR_BASE_URL,
                CENTOS_DEVELOPMENT_COMPOSE_BASE_URL,
            ],
            {"mirror.stream.centos.org": 0, "composes.stream.centos.org": 1},
        ),
    ],
)
def test_repository_priorities(mirror_list, expected):
    """Test that the resulting mapping of priorities for a set of repo URLs matches expectation."""
    actual = _repository_priorities(mirror_list)
    assert expected == actual


@pytest.mark.parametrize(
    "mirror_list,expected",
    [
        ([], set()),
        (
            ["https://buildlogs.centos.org/9-stream/automotive/{arch}/packages-main/"],
            {
                "https://buildlogs.centos.org/9-stream/automotive/x86_64/packages-main/",
                "https://buildlogs.centos.org/9-stream/automotive/aarch64/packages-main/",
            },
        ),
        (
            [
                f"{CENTOS_MIRROR_BASE_URL}/{{centos_repo}}/{{arch}}/os/",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/{{centos_repo}}/{{arch}}/os/",
                f"{CENTOS_MIRROR_BASE_URL}/{{centos_repo}}/source/tree/Packages",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/{{centos_repo}}/source/tree/Packages",
            ],
            {
                f"{CENTOS_MIRROR_BASE_URL}/BaseOS/x86_64/os/",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/BaseOS/x86_64/os/",
                f"{CENTOS_MIRROR_BASE_URL}/AppStream/x86_64/os/",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/AppStream/x86_64/os/",
                f"{CENTOS_MIRROR_BASE_URL}/CRB/x86_64/os/",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/CRB/x86_64/os/",
                f"{CENTOS_MIRROR_BASE_URL}/BaseOS/aarch64/os/",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/BaseOS/aarch64/os/",
                f"{CENTOS_MIRROR_BASE_URL}/AppStream/aarch64/os/",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/AppStream/aarch64/os/",
                f"{CENTOS_MIRROR_BASE_URL}/CRB/aarch64/os/",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/CRB/aarch64/os/",
                f"{CENTOS_MIRROR_BASE_URL}/BaseOS/source/tree/Packages",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/BaseOS/source/tree/Packages",
                f"{CENTOS_MIRROR_BASE_URL}/AppStream/source/tree/Packages",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/AppStream/source/tree/Packages",
                f"{CENTOS_MIRROR_BASE_URL}/CRB/source/tree/Packages",
                f"{CENTOS_DEVELOPMENT_COMPOSE_BASE_URL}/CRB/source/tree/Packages",
            },
        ),
    ],
)
def test_expand_repository_urls(mirror_list, expected):
    """Test expanding repository URLs with placeholders matches expectation."""
    actual = _expand_repository_urls(mirror_list)
    assert expected == actual
