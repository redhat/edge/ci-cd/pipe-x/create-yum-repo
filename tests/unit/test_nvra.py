"""Tests for create_yum_repo.nvra"""
from asyncio import PriorityQueue
from collections import defaultdict

import pytest
import respx

from create_yum_repo.nvra import (
    _index_repo,
)
from create_yum_repo.rpm_source import RpmSource


@pytest.mark.respx(
    base_url="http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/repodata/"
)
@pytest.mark.asyncio
async def test_index_repo(respx_mock, async_client, repomd_xml_path, primary_xml_path):
    nvra = "glibc-devel-2.34-32.el9.aarch64"
    queue = PriorityQueue()
    queue.put_nowait(
        RpmSource(
            uri=f"http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/Packages/{nvra}.rpm",
            priority=0,
        )
    )
    expected = {nvra: queue}
    actual = defaultdict(PriorityQueue)

    with repomd_xml_path.open("r") as fake_repomd, primary_xml_path.open(
        "rb"
    ) as fake_primary:
        respx_mock.get("repomd.xml").respond(status_code=200, text=fake_repomd.read())
        respx_mock.get(
            "05a02378d6c8959f7011a33bd45f65e1fb438aa4f00327ab782601067cc778e5-primary.xml.gz"
        ).respond(status_code=200, content=fake_primary.read())

    await _index_repo(
        nvras=actual,
        repo="http://mirror.stream.centos.org/9-stream/BaseOS/x86_64/os/",
        priority_mapping={"mirror.stream.centos.org": 0},
        client=async_client,
    )

    for key, value in expected.items():
        assert key in actual
        # This is a hack to compare two PriorityQueues. Apparently
        # 2 PriorityQueue objects compare equal only if they are the same
        # object, doesn't matter if they contain the same items or not.
        assert actual[key]._queue == value._queue
