"""Tests for create_yum_repo.external_commands"""
import subprocess
from pathlib import Path
from unittest.mock import patch

import pytest

from create_yum_repo.external_commands import (
    curl,
    dnf_makecache,
    dnf_download_package,
    rpm_checksig,
    create_repo,
    create_index_html,
)


@patch("create_yum_repo.external_commands.subprocess.run")
def test_create_repo(mock_subprocess_run):
    """Test create_repo"""
    command = "createrepo_c /tmp/repodir"
    expected = command.split()
    create_repo(Path("/tmp/repodir"))
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@patch("create_yum_repo.external_commands.subprocess.run")
def test_create_index_html(mock_subprocess_run):
    """Test create_index_html"""
    command = "tree -H . /tmp/repodir -o /tmp/repodir/index.html"
    expected = command.split()
    create_index_html(Path("/tmp/repodir"))
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@pytest.mark.parametrize(
    "repos,conf_file,verbose,command",
    [
        ((), None, False, "dnf makecache"),
        (
            (),
            Path("/var/lib/repos/dnf.conf"),
            True,
            "dnf -v -c /var/lib/repos/dnf.conf makecache",
        ),
        (
            ("auto",),
            Path("/var/lib/repos/dnf.conf"),
            False,
            "dnf -c /var/lib/repos/dnf.conf --repo=auto makecache",
        ),
        (
            ("auto", "auto-src"),
            Path("/var/lib/repos/dnf.conf"),
            False,
            "dnf -c /var/lib/repos/dnf.conf --repo=auto --repo=auto-src makecache",
        ),
    ],
)
@patch("create_yum_repo.external_commands.subprocess.run")
def test_dnf_makecache(mock_subprocess_run, repos, conf_file, verbose, command):
    """Test dnf_makecache"""
    expected = command.split(" ")
    dnf_makecache(*repos, conf_file=conf_file, verbose=verbose)
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@pytest.mark.parametrize(
    "repos,match_string,source_pkg,destdir,conf_file,verbose,command",
    [
        ((), "glibc", False, "/tmp", None, False, "dnf --destdir /tmp download glibc"),
        (
            (),
            "glibc",
            False,
            "/tmp",
            Path("/var/lib/repos/dnf.conf"),
            True,
            "dnf -v -c /var/lib/repos/dnf.conf --destdir /tmp download glibc",
        ),
        (
            ("auto",),
            "glibc",
            False,
            "/tmp",
            Path("/var/lib/repos/dnf.conf"),
            False,
            "dnf -c /var/lib/repos/dnf.conf --repo=auto --destdir /tmp download glibc",
        ),
        (
            ("auto", "auto-src"),
            "glibc",
            False,
            "/tmp",
            Path("/var/lib/repos/dnf.conf"),
            False,
            # pylint: disable=line-too-long
            "dnf -c /var/lib/repos/dnf.conf --repo=auto --repo=auto-src --destdir /tmp download glibc",
        ),
        (
            ("auto", "auto-src"),
            "glibc",
            True,
            "/tmp",
            Path("/var/lib/repos/dnf.conf"),
            False,
            # pylint: disable=line-too-long
            "dnf -c /var/lib/repos/dnf.conf --repo=auto --repo=auto-src --source --destdir /tmp download glibc",
        ),
    ],
)
@patch("create_yum_repo.external_commands.subprocess.run")
# pylint: disable=too-many-arguments
def test_dnf_download_package(
    mock_subprocess_run,
    repos,
    match_string,
    source_pkg,
    destdir,
    conf_file,
    verbose,
    command,
):
    """Test dnf_download_package"""
    expected = command.split(" ")
    dnf_download_package(
        *repos,
        match_string=match_string,
        source_pkg=source_pkg,
        destdir=destdir,
        conf_file=conf_file,
        verbose=verbose,
    )
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@patch("create_yum_repo.external_commands.subprocess.run")
def test_curl(mock_subprocess_run):
    """Test curl"""
    command = "curl --fail http://fake.url/some-path"
    expected = command.split()
    curl("http://fake.url/some-path")
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )


@patch("create_yum_repo.external_commands.subprocess.run")
def test_rpm_checksig(mock_subprocess_run):
    """Test rpm_checksig"""
    fake_rpm = "my-fake-rpm-1.0-16.el9.aarch64.rpm"
    command = f"rpm --checksig {fake_rpm}"
    expected = command.split()
    rpm_checksig(fake_rpm)
    mock_subprocess_run.assert_called_once_with(
        expected, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=True
    )
