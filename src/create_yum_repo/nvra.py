"""Functions for nvra"""

import asyncio
import gzip
import logging
from asyncio import PriorityQueue
from collections import defaultdict
from io import BytesIO
from typing import MutableMapping, Mapping, Collection, BinaryIO, Iterable, Tuple
from urllib.parse import urljoin, urlparse
from xml.etree.ElementTree import fromstring, Element, XMLPullParser

import httpx

from .download import async_client
from .rpm_source import RpmSource

REPOMD_XML_NAMESPACES = {"": "http://linux.duke.edu/metadata/repo"}
PRIMARY_XML_NAMESPACES = {"": "http://linux.duke.edu/metadata/common"}
PRIMARY_XML_CHUNK_SIZE = 8192  # 8KB

ARBITRARILY_LOW_PRIORITY = 999


async def build_nvra_lookup(
    repos: Collection[str], priority_mapping: Mapping[str, int]
) -> Mapping[str, PriorityQueue]:
    """Build a mapping from package NVRA to its download URLs for every package in each repo.

    Notes:
        The order in which repo URLs are passed to the main entrypoint imposes a natural
        priority order. In other words, if repo_url_a is passed before repo_url_b it means
        that if a package is available from both repo_url_a and repo_url_b then we should
        prefer to download it from repo_url_a.

        In order to preserve this priority ordering and also parallelize repository indexing,
        each NVRA is mapped to a PriorityQueue of RpmSource objects rather than a single URI.
        RpmSource objects encapsulate an RPM's download URI and the priority given to that
        URI.

        The priority is determined by looking up a URI's "netloc" property (from urlparse)
        in `priority_mapping` (using an arbitrarily high value - indicating a low priority
        - if not present).

        Later the highest priority RpmSource is popped off of a package's queue, and the URI
        associated with that object is tried for download.

        Why are low numbers used to indicate high priority? The heap implementation in the
        standard library is a min-heap by default, meaning the values in the heap are ordered
        based on a `<` relationship, and the top of the heap is always the minimum value.

    Parameters:
        repos: A collection of repo URLs to index.
        priority_mapping: A mapping from repository domain to its priority value.
            This is used to ensure priority order for downloading packages is
            not lost while indexing asynchronously.

    Returns:
        The NVRA -> PriorityQueue mapping.
    """
    nvras = defaultdict(PriorityQueue)
    # increase timeout from default to 60.0s, since buildlogs.centos.org/autosd
    # seems to be much slower than the others
    client = async_client(timeout=60.0)
    tasks = [
        asyncio.create_task(_index_repo(nvras, repo, priority_mapping, client))
        for repo in repos
    ]
    try:
        await asyncio.gather(*tasks)
        return nvras
    finally:
        await client.aclose()


async def _index_repo(
    nvras: MutableMapping[str, PriorityQueue],
    repo: str,
    priority_mapping: Mapping[str, int],
    client: httpx.AsyncClient,
):
    # pylint: disable=too-many-locals
    domain = urlparse(repo).netloc
    priority = priority_mapping.get(domain, ARBITRARILY_LOW_PRIORITY)

    # Avoid pulling lots of deps by just parsing the xml directly
    # Find primary.xml via repomd.xml
    repomd_url = urljoin(repo, "repodata/repomd.xml")
    repomd = await _fetch_repomd_xml(repomd_url, client)
    primary_xml_path = _primary_xml_path_from_repomd(repomd)
    logging.info("Primary xml at path %s", primary_xml_path)

    full_primary_xml_url = urljoin(repo, primary_xml_path)
    logging.info("Handling primary XML from %s", full_primary_xml_url)

    compressed = await _fetch_primary_xml(full_primary_xml_url, client)
    with gzip.open(compressed) as decompressed:
        parser = XMLPullParser()
        while chunk := decompressed.read(PRIMARY_XML_CHUNK_SIZE):
            parser.feed(chunk)
            await _process_primary_xml_events(
                events=parser.read_events(), repo=repo, priority=priority, nvras=nvras
            )


async def _fetch_repomd_xml(url: str, client: httpx.AsyncClient) -> Element:
    logging.info("Fetching repomd: %s", url)
    response = await client.get(url)
    response.raise_for_status()
    root = fromstring(response.content)
    await response.aclose()
    return root


def _primary_xml_path_from_repomd(repomd: Element) -> str:
    primary_tag = repomd.find(
        "data[@type='primary']/location", namespaces=REPOMD_XML_NAMESPACES
    )
    return primary_tag.attrib["href"]


async def _fetch_primary_xml(url: str, client: httpx.AsyncClient) -> BinaryIO:
    logging.info("Fetching primary XML: %s", url)
    response = await client.get(url)
    response.raise_for_status()
    stream = BytesIO(response.content)
    await response.aclose()
    return stream


async def _process_primary_xml_events(
    events: Iterable[Tuple[str, Element]],
    repo: str,
    priority: int,
    nvras: MutableMapping[str, PriorityQueue],
):
    """Process XMLPullParser events responding to when </package> occurs.

    Parameters:
        events: An iterable of events from :py:meth:`XMLPullParser#read_events`.
        repo: The source YUM repository URL.
        priority: The priority value assigned to the domain of ``repo``.
        nvras: The NVRA lookup table.

    Returns:
        None. The NVRA lookup table is modified in-place.
    """
    # process any xml events that have occurred since the last
    # time the parser was fed
    for event, element in events:
        if event == "end" and element.tag == f"{{{PRIMARY_XML_NAMESPACES['']}}}package":
            name = element.find("name", namespaces=PRIMARY_XML_NAMESPACES)
            version = element.find("version", namespaces=PRIMARY_XML_NAMESPACES)
            arch = element.find("arch", namespaces=PRIMARY_XML_NAMESPACES)
            location = element.find("location", namespaces=PRIMARY_XML_NAMESPACES)

            nvra = f"{name.text}-{version.attrib['ver']}-{version.attrib['rel']}.{arch.text}"
            rpm_source = RpmSource(
                urljoin(repo, location.attrib["href"]), priority=priority
            )
            await nvras[nvra].put(rpm_source)

            # clear this package element now that we've processed it
            # this helps save memory as the pull parser continues to
            # be fed new data
            element.clear()
